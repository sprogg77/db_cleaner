#!/usr/bin/env python3

import csv
import sys
from collections import OrderedDict
from pprint import pprint

cols = {'email':0,
        'foren':1,
        'surn':2,
        'alum':3,
        'type':4,
        'proj':5,
        'year':6,
        'addr':7,
        'addr2':8,
        'country':9,
        'pcode':10,
        'mob':11,
        'phone':12,
        'org':13,
        'refer':14,
        'notes':15}
cols = OrderedDict(sorted(cols.items(), key=lambda t: t[1]))

def main():
  if len(sys.argv) != 3:
    exit('Please give input and output .csv files as arguments.')
  infilename = sys.argv[1]
  outfilename = sys.argv[2]
  try:
    infile = open(infilename, 'r', encoding="ISO-8859-1")
  except:
    exit('Failed to open input .csv file.')

  spreadsheet = csv.reader(infile)
  header_str = get_header(spreadsheet)
  person_list, duplicate_rows_list = get_person_list(spreadsheet)
  for person in person_list:
    for row in duplicate_rows_list:
      if person.email == row[cols['email']]:
        person.merge_additional_rows(row)

  for person in person_list:
    person.check_name()

  infile.close()
  write_csv(header_str, outfilename, person_list)

  

class Person(object):
  def __init__(self, row):
    for col, entry in zip(cols.keys(), row):
      setattr(self, col, entry)
    
  def check_name(self):
    # forename,
    # forename, surname 
    # forename surname, surname
    # title forename, surname
    # title forename surname, surname
    titles = ['dr', 'prof', 'lord', 'lady', 'sir', 'rev', 'mr', 'mrs', 'ms', 'miss']
    print(self.foren)
    if len(self.foren.split()) > 1:
      if (self.surn == '') and not (self.foren.split()[0].lower() in titles):
        print('Surname field blank, replacing with: {}'.format(self.foren.split()[-1]))
        self.surn = self.foren.split()[-1]
      if self.surn.lower() in self.foren.lower():
        print('Surname in forename field, removing superfluous surname: {} from: {}'.format(self.foren.split()[-1], self.foren))
        self.foren = self.foren.split()[:-1][0]

  def merge_additional_rows(self, row):
    for col, new_entry in zip(cols.keys(), row):
      if (getattr(self, col) == '') and (new_entry != ''):
        setattr(self, col, new_entry)

  def get_csv_row(self):
    row = [getattr(self, col) for col in cols.keys()]
    print(row)
    return ','.join(row) + '\r'
    

def write_csv(header_str, outfilename, person_list):
  outfile = open(outfilename, 'w', encoding="ISO-8859-1")
  outfile.write(header_str)
  for person in person_list:
    outfile.write(person.get_csv_row())

def get_header(csv_obj):
  for row in csv_obj:
    return ','.join(row) + '\r'

def get_person_list(csv_obj):
  person_list = []
  email_list = []
  duplicate_rows_list = []
  for row in csv_obj:
    email = row[cols['email']]
    if not ('@' in email):
      pass
    elif (email in email_list):
      duplicate_rows_list.append(row)
    else:
      email_list.append(email)
      person_list.append(Person(row))
  print('Unique people: ', len(person_list))
  print('Duplicate entries: ', len(duplicate_rows_list))
  return person_list, duplicate_rows_list

if __name__ == '__main__':
  main()
